/**
 * Copyright JS Foundation and other contributors, http://js.foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

module.exports = function (RED) {
  "use strict";
  var request = require("request");
  var mustache = require("mustache");
  var querystring = require("querystring");
  var cookie = require("cookie");
  var hashSum = require("hash-sum");

  function FaceWebHook(config) {
    RED.nodes.createNode(this, config);
    var node = this;
    var nodeUrl = config.url;
    var nodeMethod = config.method || "POST";
    if (config.tls) {
      var tlsNode = RED.nodes.getNode(config.tls);
    }
    this.ret = config.ret || "txt";
    this.authType = config.authType || "basic";
    this.includeImage = config.includeImage || false
    if (RED.settings.httpRequestTimeout) {
      this.reqTimeout = parseInt(RED.settings.httpRequestTimeout) || 120000;
    } else {
      this.reqTimeout = 120000;
    }

    this.on("input", function (msg, nodeSend, nodeDone) {
      msg.payload = {
        detected: msg.payload,
      }
      if(this.includeImage) {
        msg.payload.image = msg.image
      }
      var preRequestTimestamp = process.hrtime();
      node.status({ fill: "blue", shape: "dot", text: "status.sending" });
      var url = nodeUrl;

      if (!url) {
        node.error(RED._("error.no-url"), msg);
        nodeDone();
        return;
      }
      // url must start http:// or https:// so assume http:// if not set
      if (url.indexOf("://") !== -1 && url.indexOf("http") !== 0) {
        node.warn(RED._("error.invalid-transport"));
        node.status({ fill: "red", shape: "ring", text: "error.invalid-transport" });
        nodeDone();
        return;
      }

      if (!(url.indexOf("http://") === 0 || url.indexOf("https://") === 0)) {
        url = `${ tlsNode ? 'https://' : "http://" }${url}`
      }

      var method = nodeMethod.toUpperCase() || "POST";
      var isHttps = /^https/i.test(url);

      var opts = {
        url: url,
        defaultPort: isHttps ? 443 : 80,
        timeout: node.reqTimeout,
        method: method,
        headers: {
          'User-Agent': "Nilvana-Agent"
        },
        encoding: null,
        maxRedirects: 4,
        jar: request.jar(),
        forever: false,
        timeout:5000
      };

      var ctSet = "Content-Type"; // set default camel case
      var clSet = "Content-Length";

      var redirectList = [];

      if (!opts.hasOwnProperty("followRedirect") || opts.followRedirect) {
        opts.followRedirect = function (res) {
          var redirectInfo = {
            location: res.headers.location,
          };
          if (res.headers.hasOwnProperty("set-cookie")) {
            redirectInfo.cookies = extractCookies(res.headers["set-cookie"]);
          }
          redirectList.push(redirectInfo);
          if (this.headers.cookie) {
            delete this.headers.cookie;
          }
          return true;
        };
      }

      if (opts.headers.hasOwnProperty("cookie")) {
        var cookies = cookie.parse(opts.headers.cookie, { decode: String });
        for (var name in cookies) {
          opts.jar.setCookie(
            cookie.serialize(name, cookies[name], { encode: String }),
            url
          );
        }
        delete opts.headers.cookie;
      }

      if (this.credentials) {
        if(this.credentials.user) {
          opts.auth = {
            user: this.credentials.user,
            pass: this.credentials.password || "",
          };
          if (this.authType === "digest") {
            opts.auth.sendImmediately = false
          }
        } else if (this.authType === "bearer") {
          opts.auth = {
            bearer: this.credentials.password || "",
          };
        }
      }
      var payload = null;

      if (["GET", "HEAD"].indexOf(method) === -1 && typeof msg.payload !== "undefined") {
        if (opts.headers["content-type"] == "multipart/form-data" && typeof msg.payload === "object") {
          opts.formData = {};

          for (var opt in msg.payload) {
            if (msg.payload.hasOwnProperty(opt)) {
              var val = msg.payload[opt];
              if (val !== undefined && val !== null) {
                if (typeof val === "string" || Buffer.isBuffer(val)) {
                  opts.formData[opt] = val;
                } else if (typeof val === "object" && val.hasOwnProperty("value")) {
                  // Treat as file to upload - ensure it has an options object
                  // as request complains if it doesn't
                  if (!val.hasOwnProperty("options")) {
                    val.options = {};
                  }
                  opts.formData[opt] = val;
                } else {
                  opts.formData[opt] = JSON.stringify(val);
                }
              }
            }
          }
        } else {
          if (typeof msg.payload === "string" || Buffer.isBuffer(msg.payload)) {
            payload = msg.payload;
          } else if (typeof msg.payload == "number") {
            payload = msg.payload + "";
          } else {
            if ( opts.headers["content-type"] == "application/x-www-form-urlencoded") {
              payload = querystring.stringify(msg.payload);
            } else {
              payload = JSON.stringify(msg.payload);
              if (opts.headers["content-type"] == null) {
                opts.headers[ctSet] = "application/json";
              }
            }
          }

          if (opts.headers["content-length"] == null) {
            if (Buffer.isBuffer(payload)) {
              opts.headers[clSet] = payload.length;
            } else {
              opts.headers[clSet] = Buffer.byteLength(payload);
            }
          }
          opts.body = payload;
        }
      }

      if (method == "GET" && typeof msg.payload !== "undefined") {
        if (typeof msg.payload === "object") {
          try {
            if (opts.url.indexOf("?") !== -1) {
              opts.url +=
                (opts.url.endsWith("?") ? "" : "&") +
                querystring.stringify(msg.payload);
            } else {
              opts.url += "?" + querystring.stringify(msg.payload);
            }
          } catch (err) {
            node.error(RED._("error.invalid-payload"), msg);
            nodeDone();
            return;
          }
        } else {
          node.error(RED._("error.invalid-payload"), msg);
          nodeDone();
          return;
        }
      }

      // revert to user supplied Capitalisation if needed.
      if (opts.headers.hasOwnProperty("content-type") && ctSet !== "content-type") {
        opts.headers[ctSet] = opts.headers["content-type"];
        delete opts.headers["content-type"];
      }

      if (opts.headers.hasOwnProperty("content-length") && clSet !== "content-length") {
        opts.headers[clSet] = opts.headers["content-length"];
        delete opts.headers["content-length"];
      }

      if (tlsNode) {
        tlsNode.addTLSOptions(opts);
      } else {
        if (msg.hasOwnProperty("rejectUnauthorized")) {
          opts.rejectUnauthorized = msg.rejectUnauthorized;
        }
      }

      function errorHandler (err) {
        if (err.code === "ETIMEDOUT" || err.code === "ESOCKETTIMEDOUT") {
          node.error(RED._("error.no-response"), msg);
          node.status({
            fill: "red",
            shape: "ring",
            text: "error.no-response",
          });
        } else {
          node.error(err, msg);
          node.status({ fill: "red", shape: "ring", text: err.code });
        }
        msg.payload = err.toString() + " : " + url;
        msg.statusCode = err.code;
        nodeSend(msg);
        nodeDone();
      }

      function responseHandler(res, body) {
        msg.statusCode = res.statusCode;
        msg.headers = res.headers;
        msg.responseUrl = res.request.uri.href;
        msg.payload = body;
        msg.redirectList = redirectList;

        if (msg.headers.hasOwnProperty("set-cookie")) {
          msg.responseCookies = extractCookies(msg.headers["set-cookie"]);
        }
        msg.headers["x-node-red-request-node"] = hashSum(msg.headers);
        // msg.url = url;   // revert when warning above finally removed
        if (node.metric()) {
          // Calculate request time
          var diff = process.hrtime(preRequestTimestamp);
          var ms = diff[0] * 1e3 + diff[1] * 1e-6;
          var metricRequestDurationMillis = ms.toFixed(3);
          node.metric("duration.millis", msg, metricRequestDurationMillis);
          if (res.client && res.client.bytesRead) {
            node.metric("size.bytes", msg, res.client.bytesRead);
          }
        }

        // Convert the payload to the required return type
        if (node.ret !== "bin") {
          msg.payload = msg.payload.toString("utf8"); // txt
          if (node.ret === "obj") {
            try {
              msg.payload = JSON.parse(msg.payload);
            } catch (e) {
              // obj
              node.warn(RED._("error.json-error"));
            }
          }
        }
        node.status({});
        nodeSend(msg);
        nodeDone();
      }
      request(opts, function (err, res, body) {
        if (err) {
          errorHandler(err)
        } else {
          responseHandler(res, body);
        }
      });
    });

    this.on("close", function () {
      node.status({});
    });

    function extractCookies(setCookie) {
      var cookies = {};
      setCookie.forEach(function (c) {
        var parsedCookie = cookie.parse(c);
        var eq_idx = c.indexOf("=");
        var key = c.substr(0, eq_idx).trim();
        parsedCookie.value = parsedCookie[key];
        delete parsedCookie[key];
        cookies[key] = parsedCookie;
      });
      return cookies;
    }
  }

  RED.nodes.registerType("facial-webhooks", FaceWebHook, {
    credentials: {
      user: { type: "text" },
      password: { type: "password" },
    },
  });

  RED.httpAdmin.post("/facial-webhooks/test/:id", RED.auth.needsPermission("facewebhook.write"), function(req,res) {
    // var node = RED.nodes.getNode(req.params.id);
    console.log(req);
    // if (node != null) {
    //     try {
    //         node.receive();
    //         res.sendStatus(200);
    //     } catch(err) {
    //         res.sendStatus(500);
    //         // node.error(RED._("inject.failed",{error:err.toString()}));
    //     }
    // } else {
    //     res.sendStatus(404);
    // }
  });
};
# Face webhook node

## Get Started

```shell
npm install --save https://gitlab.com/nilvana-ai/edge/face-webhook-node.git
```

## Options

`Request Method`: HTTP Request Method [POST, PUT, GET, DELETE, HEAD]

`URL`: Remote Webhook Request URL

`Enable secure (SSL/TLS) connection`: Enable HTTPS request

`Use authentication`: HTTP Authentication [Basic, Digest, Bearer Token]
  * [Basic](https://tools.ietf.org/html/rfc7617): Authentiate by Username and Password
  * [Digest](https://tools.ietf.org/html/rfc7616)
  * [Bearer](https://tools.ietf.org/html/rfc6750): OAuth 2.0 Authorization
  * Ref: https://developer.mozilla.org/zh-TW/docs/Web/HTTP/Authentication

`Return`: Define which `data format` [UTF-8(Text), Binary, JSON] will return from Remote webhook server, and output to next Node-RED node

`Include detected image`: Include detected Image (An image encoded by Base64)

```
{
    detected: [
        {}
    ],
    image: "<base64>"
}
```


## Webhook JSON Structure

```
{
    detected: [
        {
            name: <String>,
            dist: <Float>,
            count: <Number>,
            box: {
                x: <Number>,
                y: <Number>,
                w: <Number>,
                h: <Number>
            }
        }
    ],
    image: <Base64 (Optional)>
}
```

**Exampe**:

```
{

    "detected": [
        {
            "name":"Sam",
            "dist":0.11234,
            "count":3,
            "box":{
                "x":277,
                "y":598,
                "w":277,
                "h":598
            }
        },
        {
            "name":"Kevin",
            "box":{
                "x":277,
                "y":598,
                "w":277,
                "h":598
            }
        },
        {
            "name":"Chris",
            "box":{
                "x":277,
                "y":598,
                "w":277,
                "h":598
            }
        }
    ],
    "image":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAbCAYAAACN1PRVAAAACXBIWXMAAAsSAAALEgHS3X78AAAAjElEQVRIie2WwQnAIAxFv6WD2E0cTTdzk3aTlKJX+V9KpYcEJJeHD6KJBjPDqtiWmVzmsl/J9hk4ABFAQssXgGota/E0tbJglgdgnthDgkaiKSEdV710p1Cmg5VUuSBJPBHKKbIoyiinyNTbRjlFVkUZ5aisH3ohWJH6bWWfTb3UbyeIfwtc5rKPA8ANwjQPBSWDe/EAAAAASUVORK5CYII="
}
```

---

## Copyright and license

Copyright inwinSTACK Inc. under [the Apache 2.0 license](LICENSE.md).
